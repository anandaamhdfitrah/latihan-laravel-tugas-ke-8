<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio()
    {
        return view('halaman.register');
    }

    public function kirim(Request $request)
    {
        //dd($request->all());
        $namaDepan = $request['Nama_Depan'];
        $namaBelakang = $request['Nama_Belakang'];
        $bio = $request['bio'];

        return view('halaman.welcome', compact('namaDepan','namaBelakang',"bio"));
     }
}
