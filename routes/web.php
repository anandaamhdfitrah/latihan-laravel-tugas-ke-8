<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@bio');
Route::post('/kirim', 'AuthController@kirim');

Route::get('/data-tables', 'IndexController@table');

//CRUD Cast
//Create
Route::get('/cast/create', 'CastController@create'); //Mengarah ke form tambah data
Route::get('/cast', 'CastController@store'); //Menyimpan data form ke database table cast

//Read
Route::get('/cast', 'CastController@index'); //Ambil data ke database ditampilkan diblade
Route::get('/cast/{cast_id})', 'CastController@show'); //Route detail cast

//Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //Route untuk mengerah ke form edit
Route::put('/cast/{cast_id}', 'CastController@update');

//Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); //Route delete data berdasarkan id