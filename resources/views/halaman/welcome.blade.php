@extends('layout.master')
@section('title')
Halaman Home
@endsection
@section('content')
    <h1>Selamat Datang! {{$namaDepan}} {{$namaBelakang}} {{$bio}}</h1>
    <h3>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</h3>
@endsection